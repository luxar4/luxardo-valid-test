DROP TABLE IF EXISTS user;
 
CREATE TABLE user (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  email VARCHAR(250) NOT NULL,
  password VARCHAR(250) NOT NULL,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,

  UNIQUE(email)
);
 
INSERT INTO user (email, password, first_name, last_name) VALUES
  ('luxardo.asis@gmail.com', '$2a$10$FPvBI1M6iEoSshS4DWmA5OGQSUxZ5DLhZKtBCljVyX28twoai7sre', 'Luxardo Rafael', 'Asis Ramirez'),
  ('lola.mento@gmail.com', '$2a$10$Gx8WZWTd6PUhDn.DqpUmouFhmUmzmhexDOvwfvAedA3WkFT4GUvjy', 'Lola', 'Mento'),
  ('karen.latada@gmail.com', '$2a$10$khNgfOn6uK1uuWRiqdAdDeXNUySQdGF0riSbLbfVslREyQXrquyW2', 'Karen', 'Latada');
