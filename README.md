# Solución prueba técnica VALID

### 1) Prerequisitos
* JDK 8

### 2) Descripción
El proyecto está desarrollado en **Java** 8 - `1.8.0_201` y **Gradle** 6.3, se monta sobre el **puerto 4000**, este no se debe encontrar en uso, si es este el caso puede modificarlo en `src/main/resources/application.yml` - propiedad `server.port`, en el proyecto encontrará la colección de **Postman** con la cual podrá probar lo endpoints `VALID Test.postman_collection.json`.

### 3) Instrucciones de ejecución
* **Clonar:** `git clone git@gitlab.com:luxar4/luxardo-valid-test.git`
* **Compilar:** `./gradlew clean build` - este comando se ejecuta en la raíz del proyecto
* **Ejecutar:** `java -jar build/libs/luxardo-valid-test-0.0.1-SNAPSHOT.jar` - este comando se ejecuta en la raíz del proyecto

### 4) Buenas prácticas
* **Pruebas unitarias:** se incluyó pruebas unitarias, estas solo se agregaron al endpoint de login.
* **Arquitectura:** se desarrolló bajo una arquitectura **MVC**, en tres capas (agregando una capa de lógica de negocio -**service**- entre las capas de presentación -**controller**- y la capa de data -**repository y entity**-):
  * **Controller:** presentación al cliente - con el patrón de diseño **DTO** para los contratos Request y Response.
  * **Service:** lógica de negocio.
  * **Repository y Entity:** coneccion a base de datos - con el patrón de diseño **DAO** para las entidades.

### 5) Seguridad
* **Spring security:** se agregó al proyecto esta librería, lo cual ofrece una capa básica de seguridad.
* **Bcrypt:** cifrado de las contraseñas de los usuarios con este algoritmo y con un round de 10.
* **JPA:** se utilizó esta librería para la coneccion a base de datos, esta librería ya resuelve una de las vulnerabilidades más comunes como lo es **SQL Injection**
* **Sesiones:** se simuló un sistema de sesiones con con un singleton, para evitar que un usuario pueda acceder a información que no le corresponda.

### 6) Servicios
#### Login
* **Method:** POST
* **Endpoint:** http://localhost:4000/api/v1/test/auth/login
* **Request:** `{ "email": "luxardo.asis@gmail.com", "password": "luxardo.asis" }`
* **Response:** `{ "code": 0, "message": "OK", "data": { "sessionId": "96af36d2-0078-43ad-885c-47472ca44b07" } }`
#### Logout
* **Method:** GET
* **Endpoint:** http://localhost:4000/api/v1/test/auth/logout
* **Headers:** `session-id=96af36d2-0078-43ad-885c-47472ca44b07`
* **Response:** `{ "code": 0, "message": "OK" }`
#### Create User
* **Method:** POST
* **Endpoint:** http://localhost:4000/api/v1/test/user
* **Request:** `{ "email": "helen.chufe@gmail.com", "password": "Hc12345@", "firstName": "Helen", "lastName": "Chufe" }`
* **Response:** `{ "code": 0, "message": "OK", "data": { "sessionId": "8bc0ebbd-2729-4e6b-b6c5-245e89a6c796" } }`
#### Get User
* **Method:** GET
* **Endpoint:** http://localhost:4000/api/v1/test/user
* **Headers:** `session-id=8bc0ebbd-2729-4e6b-b6c5-245e89a6c796`
* **Response:** `{ "code": 0, "message": "OK", "data": { "email": "helen.chufe@gmail.com", "firstName": "Helen", "lastNAme": "Chufe" } }`


