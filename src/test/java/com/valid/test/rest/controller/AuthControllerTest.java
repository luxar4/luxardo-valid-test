
package com.valid.test.rest.controller;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.valid.test.LuxardoValidTestApplication;
import com.valid.test.constant.StatusCode;
import com.valid.test.rest.controller.dto.AuthLoginRequest;
import com.valid.test.rest.controller.dto.AuthLoginResponse;
import com.valid.test.rest.controller.dto.GenericResponse;
import com.valid.test.rest.handler.exception.InvalidCredentialsException;
import com.valid.test.rest.service.AuthService;

@AutoConfigureMockMvc
@SpringBootTest(
        classes = LuxardoValidTestApplication.class)
class AuthControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private AuthService authService;

    @Test
    void testLogin_invalidCredentials() throws Exception {
        AuthLoginRequest request = new AuthLoginRequest();
        request.setEmail("helen.chufe@gmail.com");
        request.setPassword("Hc12345@");

        given(authService.login(Mockito.any()))
                .willThrow(new InvalidCredentialsException("invalid email and/or password"));

        mvc.perform(post("/auth/login").content(new ObjectMapper().writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isUnauthorized())
                .andExpect(jsonPath("code").value(StatusCode.UNAUTHORIZED.getCode()))
                .andExpect(jsonPath("message").value("invalid email and/or password"))
                .andExpect(jsonPath("data").doesNotExist());
    }

    @Test
    void testLogin_OK() throws Exception {
        AuthLoginResponse data = new AuthLoginResponse();
        data.setSessionId("session-id");

        GenericResponse<AuthLoginResponse> response = new GenericResponse<>();
        response.setCode(StatusCode.OK.getCode());
        response.setMessage(StatusCode.OK.getMessage());
        response.setData(data);

        AuthLoginRequest request = new AuthLoginRequest();
        request.setEmail("helen.chufe@gmail.com");
        request.setPassword("Hc12345@");

        given(authService.login(Mockito.any())).willReturn(response);

        mvc.perform(post("/auth/login").content(new ObjectMapper().writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk())
                .andExpect(jsonPath("code").value(StatusCode.OK.getCode()))
                .andExpect(jsonPath("message").value(StatusCode.OK.getMessage()))
                .andExpect(jsonPath("data.sessionId").value("session-id"));
    }
}
