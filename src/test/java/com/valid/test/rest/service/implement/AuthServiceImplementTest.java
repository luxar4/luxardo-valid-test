
package com.valid.test.rest.service.implement;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;

import com.valid.test.LuxardoValidTestApplication;
import com.valid.test.constant.StatusCode;
import com.valid.test.db.entity.UserEntity;
import com.valid.test.db.repository.UserRepository;
import com.valid.test.rest.controller.dto.AuthLoginRequest;
import com.valid.test.rest.controller.dto.AuthLoginResponse;
import com.valid.test.rest.controller.dto.GenericResponse;
import com.valid.test.rest.handler.exception.InvalidCredentialsException;
import com.valid.test.rest.service.AuthService;

@SpringBootTest(
        classes = LuxardoValidTestApplication.class)
class AuthServiceImplementTest {
    @TestConfiguration
    static class AuthServiceImplementTestContextConfiguration {
        @Bean
        public AuthService authService() {
            return new AuthServiceImplement();
        }
    }

    @Autowired
    private AuthService authService;

    @MockBean
    private UserRepository userRepository;

    @Test
    void testLogin_UserIsNull() {
        Mockito.when(userRepository.findByEmail(Mockito.anyString())).thenReturn(null);

        AuthLoginRequest request = new AuthLoginRequest();
        request.setEmail("helen.chufe@gmail.com");
        request.setPassword("Hc12345@");

        assertThatThrownBy(() -> authService.login(request))
                .isExactlyInstanceOf(InvalidCredentialsException.class);
    }

    @Test
    void testLogin_invalidPassword() {
        UserEntity helen = new UserEntity();
        helen.setEmail("helen.chufe@gmail.com");
        helen.setPassword("$2a$10$gvnMNf8euPCjdRQKiITwhufS576xWc9RCBWdGLIaJzPEVwHSmf6py");
        helen.setFirstName("Helen");
        helen.setLastName("Chufe");

        Mockito.when(userRepository.findByEmail(helen.getEmail())).thenReturn(helen);

        AuthLoginRequest request = new AuthLoginRequest();
        request.setEmail("helen.chufe@gmail.com");
        request.setPassword("");

        assertThatThrownBy(() -> authService.login(request))
                .isExactlyInstanceOf(InvalidCredentialsException.class);
    }

    @Test
    void testLogin_OK() {
        UserEntity helen = new UserEntity();
        helen.setEmail("helen.chufe@gmail.com");
        helen.setPassword("$2a$10$gvnMNf8euPCjdRQKiITwhufS576xWc9RCBWdGLIaJzPEVwHSmf6py");
        helen.setFirstName("Helen");
        helen.setLastName("Chufe");

        Mockito.when(userRepository.findByEmail(helen.getEmail())).thenReturn(helen);

        AuthLoginRequest request = new AuthLoginRequest();
        request.setEmail("helen.chufe@gmail.com");
        request.setPassword("Hc12345@");

        GenericResponse<AuthLoginResponse> response = authService.login(request);

        assertThat(response.getCode()).isEqualTo(StatusCode.OK.getCode());
        assertThat(response.getMessage()).isEqualTo(StatusCode.OK.getMessage());
        assertThat(response.getData().getSessionId()).isNotEmpty();
    }
}
