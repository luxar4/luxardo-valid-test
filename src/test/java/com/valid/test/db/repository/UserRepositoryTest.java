
package com.valid.test.db.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.valid.test.db.entity.UserEntity;

@DataJpaTest
class UserRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenFindByEmail_thenReturnUser() {
        UserEntity helen = new UserEntity();
        helen.setEmail("helen.chufe@gmail.com");
        helen.setPassword("$2a$10$gvnMNf8euPCjdRQKiITwhufS576xWc9RCBWdGLIaJzPEVwHSmf6py");
        helen.setFirstName("Helen");
        helen.setLastName("Chufe");

        entityManager.persist(helen);
        entityManager.flush();

        UserEntity found = userRepository.findByEmail(helen.getEmail());

        assertThat(found.getEmail()).isEqualTo(helen.getEmail());
        assertThat(found.getPassword()).isEqualTo(helen.getPassword());
        assertThat(found.getFirstName()).isEqualTo(helen.getFirstName());
        assertThat(found.getLastName()).isEqualTo(helen.getLastName());
    }

}
