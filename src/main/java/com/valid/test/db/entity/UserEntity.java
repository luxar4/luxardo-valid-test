
package com.valid.test.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(
        name = "user")
public class UserEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "user_seq_generator")
    @SequenceGenerator(
            name = "user_seq_generator",
            allocationSize = 1,
            initialValue = 1000)
    @Column(
            name = "id")
    private Long id;
    @Column(
            name = "email")
    private String email;
    @Column(
            name = "password")
    private String password;
    @Column(
            name = "first_name")
    private String firstName;
    @Column(
            name = "last_name")
    private String lastName;

    public UserEntity() {
    }

    public UserEntity(Long id, String email, String password, String firstName, String lastName) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserEntity other = (UserEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "UserEntity [id=" + id + ", email=" + email + ", password=" + password
                + ", firstName=" + firstName + ", lastName=" + lastName + "]";
    }
}
