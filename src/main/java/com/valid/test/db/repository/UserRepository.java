
package com.valid.test.db.repository;

import org.springframework.data.repository.CrudRepository;

import com.valid.test.db.entity.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Long> {
    public UserEntity findByEmail(String email);;
}
