package com.valid.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LuxardoValidTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(LuxardoValidTestApplication.class, args);
	}

}
