
package com.valid.test.rest.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.valid.test.constant.Constants;
import com.valid.test.rest.controller.dto.GenericResponse;
import com.valid.test.rest.controller.dto.UserCreateRequest;
import com.valid.test.rest.controller.dto.UserCreateResponse;
import com.valid.test.rest.controller.dto.UserGetResponse;
import com.valid.test.rest.service.UserService;

@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<UserCreateResponse>>
            create(@RequestBody @Valid UserCreateRequest request) {
        return ResponseEntity.ok().body(userService.create(request));
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<UserGetResponse>> get(@RequestHeader(
            name = Constants.HEADER_KEY_SESSION_ID,
            required = true) final String sessionId) {
        return ResponseEntity.ok().body(userService.get(sessionId));
    }
}
