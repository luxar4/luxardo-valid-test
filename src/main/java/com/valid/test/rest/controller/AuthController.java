
package com.valid.test.rest.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.valid.test.constant.Constants;
import com.valid.test.rest.controller.dto.AuthLoginRequest;
import com.valid.test.rest.controller.dto.AuthLoginResponse;
import com.valid.test.rest.controller.dto.GenericResponse;
import com.valid.test.rest.service.AuthService;

@RestController
@RequestMapping("auth")
public class AuthController {
    @Autowired
    private AuthService authService;

    @RequestMapping(
            value = "login",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<AuthLoginResponse>>
            login(@RequestBody @Valid AuthLoginRequest request) {
        return ResponseEntity.ok().body(authService.login(request));
    }

    @RequestMapping(
            value = "logout",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Void>> logot(@RequestHeader(
            name = Constants.HEADER_KEY_SESSION_ID,
            required = true) final String sessionId) {
        return ResponseEntity.ok().body(authService.logout(sessionId));
    }
}
