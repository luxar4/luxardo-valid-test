
package com.valid.test.rest.service;

import com.valid.test.rest.controller.dto.GenericResponse;
import com.valid.test.rest.controller.dto.UserCreateRequest;
import com.valid.test.rest.controller.dto.UserCreateResponse;
import com.valid.test.rest.controller.dto.UserGetResponse;

public interface UserService {
    public GenericResponse<UserCreateResponse> create(UserCreateRequest request);

    public GenericResponse<UserGetResponse> get(String sessionId);
}
