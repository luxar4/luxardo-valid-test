
package com.valid.test.rest.service.implement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.valid.test.constant.StatusCode;
import com.valid.test.db.entity.UserEntity;
import com.valid.test.db.repository.UserRepository;
import com.valid.test.rest.controller.dto.AuthLoginRequest;
import com.valid.test.rest.controller.dto.AuthLoginResponse;
import com.valid.test.rest.controller.dto.GenericResponse;
import com.valid.test.rest.handler.exception.InvalidCredentialsException;
import com.valid.test.rest.service.AuthService;
import com.valid.test.session.Session;
import com.valid.test.session.SessionRepository;

@Service
public class AuthServiceImplement implements AuthService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthServiceImplement.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public GenericResponse<AuthLoginResponse> login(AuthLoginRequest request) {
        String email = request.getEmail();

        LOGGER.info("AuthLogin init, email[{}]", email);

        UserEntity user = userRepository.findByEmail(email);

        LOGGER.info("AuthLogin find by email, user[{}]", user);

        if (user == null
                || !bCryptPasswordEncoder.matches(request.getPassword(), user.getPassword()))
            throw new InvalidCredentialsException("invalid email and/or password");

        Session session = new Session();
        session.setEmail(email);

        String sessionId = sessionRepository.save(session);

        AuthLoginResponse data = new AuthLoginResponse();
        data.setSessionId(sessionId);

        GenericResponse<AuthLoginResponse> response = new GenericResponse<>();
        response.setCode(StatusCode.OK.getCode());
        response.setMessage(StatusCode.OK.getMessage());
        response.setData(data);

        LOGGER.info("AuthLogin ok, response[{}]", response);

        return response;
    }

    @Override
    public GenericResponse<Void> logout(String sessionId) {
        LOGGER.info("AuthLogout init, session-id[{}]", sessionId);

        sessionRepository.delete(sessionId);

        GenericResponse<Void> response = new GenericResponse<>();
        response.setCode(StatusCode.OK.getCode());
        response.setMessage(StatusCode.OK.getMessage());

        LOGGER.info("AuthLogout ok, response[{}]", response);

        return response;
    }
}
