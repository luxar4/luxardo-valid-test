
package com.valid.test.rest.service;

import com.valid.test.rest.controller.dto.AuthLoginRequest;
import com.valid.test.rest.controller.dto.AuthLoginResponse;
import com.valid.test.rest.controller.dto.GenericResponse;

public interface AuthService {
    public GenericResponse<AuthLoginResponse> login(AuthLoginRequest request);

    public GenericResponse<Void> logout(String SessionId);
}
