
package com.valid.test.rest.service.implement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.valid.test.constant.StatusCode;
import com.valid.test.db.entity.UserEntity;
import com.valid.test.db.repository.UserRepository;
import com.valid.test.rest.controller.dto.GenericResponse;
import com.valid.test.rest.controller.dto.UserCreateRequest;
import com.valid.test.rest.controller.dto.UserCreateResponse;
import com.valid.test.rest.controller.dto.UserGetResponse;
import com.valid.test.rest.handler.exception.UserAlreadyExistException;
import com.valid.test.rest.service.UserService;
import com.valid.test.session.Session;
import com.valid.test.session.SessionRepository;

@Service
public class UserServiceImplement implements UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImplement.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public GenericResponse<UserCreateResponse> create(UserCreateRequest request) {
        String email = request.getEmail();

        LOGGER.info("UserCreate init, email[{}]", email);

        UserEntity userEntity = userRepository.findByEmail(email);
        if (userEntity != null)
            throw new UserAlreadyExistException("user already exist");

        userEntity = new UserEntity();
        userEntity.setEmail(email);
        userEntity.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));
        userEntity.setFirstName(request.getFirstName());
        userEntity.setLastName(request.getLastName());

        userRepository.save(userEntity);

        Session session = new Session();
        session.setEmail(email);

        String sessionId = sessionRepository.save(session);

        UserCreateResponse data = new UserCreateResponse();
        data.setSessionId(sessionId);

        GenericResponse<UserCreateResponse> response = new GenericResponse<>();
        response.setCode(StatusCode.OK.getCode());
        response.setMessage(StatusCode.OK.getMessage());
        response.setData(data);

        LOGGER.info("UserCreate init, response[{}]", response);

        return response;
    }

    @Override
    public GenericResponse<UserGetResponse> get(String sessionId) {
        LOGGER.info("UserGet init, session-id[{}]", sessionId);

        Session session = sessionRepository.getById(sessionId);
        UserEntity userEntity = userRepository.findByEmail(session.getEmail());

        UserGetResponse data = new UserGetResponse();
        data.setEmail(userEntity.getEmail());
        data.setFirstName(userEntity.getFirstName());
        data.setLastNAme(userEntity.getLastName());

        GenericResponse<UserGetResponse> response = new GenericResponse<>();
        response.setCode(StatusCode.OK.getCode());
        response.setMessage(StatusCode.OK.getMessage());
        response.setData(data);

        LOGGER.info("UserGet init, response[{}]", response);

        return response;
    }
}
