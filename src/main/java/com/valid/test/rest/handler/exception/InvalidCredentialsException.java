
package com.valid.test.rest.handler.exception;

public class InvalidCredentialsException extends RuntimeException {
    private static final long serialVersionUID = -1845953413409840170L;

    public InvalidCredentialsException() {
        super();
    }

    public InvalidCredentialsException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public InvalidCredentialsException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCredentialsException(String message) {
        super(message);
    }

    public InvalidCredentialsException(Throwable cause) {
        super(cause);
    }
}
