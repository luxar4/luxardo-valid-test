
package com.valid.test.rest.handler;

import java.net.BindException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.valid.test.constant.StatusCode;
import com.valid.test.rest.controller.dto.GenericResponse;
import com.valid.test.rest.handler.exception.InvalidCredentialsException;
import com.valid.test.rest.handler.exception.InvalidSessionException;
import com.valid.test.rest.handler.exception.UserAlreadyExistException;

@RestControllerAdvice
public class ResponseEntityExceptionHandler {
    private static final Logger logger =
            LoggerFactory.getLogger(ResponseEntityExceptionHandler.class);

    @ResponseStatus(
            value = HttpStatus.SERVICE_UNAVAILABLE)
    @ExceptionHandler(
            value = {
                AsyncRequestTimeoutException.class
            })
    public ResponseEntity<GenericResponse<Void>>
            serviceUnavailableExceptionHandler(HttpServletRequest request, Exception e) {
        final String message = StatusCode.SERVICE_UNAVAILABLE.getMessage();
        logger.error("ServiceUnavailableExceptionHandler", e);

        GenericResponse<Void> response = new GenericResponse<>();
        response.setMessage(message);
        response.setCode(StatusCode.SERVICE_UNAVAILABLE.getCode());

        logger.error("ServiceUnavailableExceptionHandler finally, response[{}]", response);

        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(response);
    }

    @ResponseStatus(
            value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(
            value = {
                NoHandlerFoundException.class
            })
    public ResponseEntity<GenericResponse<Void>>
            notFoundExceptionHandler(HttpServletRequest request, Exception e) {
        final String message = StatusCode.NOT_FOUND.getMessage();

        logger.error("NotFoundExceptionHandler", e);

        GenericResponse<Void> response = new GenericResponse<>();
        response.setMessage(message);
        response.setCode(StatusCode.NOT_FOUND.getCode());

        logger.error("NotFoundExceptionHandler finally, response[{}]", response);

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }

    @ResponseStatus(
            value = HttpStatus.NOT_ACCEPTABLE)
    @ExceptionHandler(
            value = {
                HttpMediaTypeNotAcceptableException.class
            })
    public ResponseEntity<GenericResponse<Void>>
            notAcceptableExceptionHandler(HttpServletRequest request, Exception e) {
        final String message = StatusCode.NOT_ACCEPTABLE.getMessage();

        logger.error("NotAcceptableExceptionHandler", e);

        GenericResponse<Void> response = new GenericResponse<>();
        response.setMessage(message);
        response.setCode(StatusCode.NOT_ACCEPTABLE.getCode());

        logger.error("NotAcceptableExceptionHandler finally, response[{}]", response);

        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response);
    }

    @ResponseStatus(
            value = HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ExceptionHandler(
            value = {
                HttpMediaTypeNotSupportedException.class
            })
    public ResponseEntity<GenericResponse<Void>>
            unsupportedMediaTypeExceptionHandler(HttpServletRequest request, Exception e) {
        final String message = StatusCode.UNSUPPORTED_MEDIA_TYPE.getMessage();

        logger.error("UnsupportedMediaTypeExceptionHandler", e);

        GenericResponse<Void> response = new GenericResponse<>();
        response.setMessage(message);
        response.setCode(StatusCode.UNSUPPORTED_MEDIA_TYPE.getCode());

        logger.error("UnsupportedMediaTypeExceptionHandler finally, response[{}]", response);

        return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).body(response);
    }

    @ResponseStatus(
            value = HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(
            value = {
                HttpRequestMethodNotSupportedException.class
            })
    public ResponseEntity<GenericResponse<Void>>
            methodNotAllowedExceptionHandler(HttpServletRequest request, Exception e) {
        final String message = StatusCode.METHOD_NOT_ALLOWED.getMessage();

        logger.error("MethodNotAllowedExceptionHandler", e);

        GenericResponse<Void> response = new GenericResponse<>();
        response.setMessage(message);
        response.setCode(StatusCode.METHOD_NOT_ALLOWED.getCode());

        logger.error("MethodNotAllowedExceptionHandler finally, response[{}]", response);

        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).body(response);
    }

    @ExceptionHandler(
            value = {
                HttpMessageNotReadableException.class,
                MissingServletRequestParameterException.class,
                ServletRequestBindingException.class,
                TypeMismatchException.class,
                MissingServletRequestPartException.class,
                BindException.class,
                UserAlreadyExistException.class
            })
    public ResponseEntity<GenericResponse<Void>>
            badRequestExceptionHandler(HttpServletRequest request, Exception e) {
        final String message =
                e instanceof UserAlreadyExistException && e.getMessage() != null ? e.getMessage()
                        : StatusCode.BAD_REQUEST.getMessage();

        logger.error("BadRequestExceptionHandler", e);

        GenericResponse<Void> response = new GenericResponse<>();
        response.setMessage(message);
        response.setCode(StatusCode.BAD_REQUEST.getCode());

        logger.error("BadRequestExceptionHandler finally, response[{}]", response);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }

    @ExceptionHandler(
            value = {
                MethodArgumentNotValidException.class
            })
    public ResponseEntity<GenericResponse<Void>> badRequestExceptionHandler(
            HttpServletRequest request, MethodArgumentNotValidException e) {
        String message = StatusCode.BAD_REQUEST.getMessage();

        if (!e.getBindingResult().getAllErrors().isEmpty())
            message = e.getBindingResult().getAllErrors().get(0).getDefaultMessage();

        logger.error("BadRequestExceptionHandler", e);

        GenericResponse<Void> response = new GenericResponse<>();
        response.setMessage(message);
        response.setCode(StatusCode.BAD_REQUEST.getCode());

        logger.error("BadRequestExceptionHandler finally, response[{}]", response);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }

    @ResponseStatus(
            value = HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(
            value = {
                InvalidSessionException.class, InvalidCredentialsException.class
            })
    public ResponseEntity<GenericResponse<Void>>
            unauthorizedExceptionHandler(HttpServletRequest request, Exception e) {
        String message =
                e.getMessage() == null ? StatusCode.UNAUTHORIZED.getMessage() : e.getMessage();

        logger.error("UnauthorizedExceptionHandler", e);

        GenericResponse<Void> response = new GenericResponse<>();
        response.setMessage(message);
        response.setCode(StatusCode.UNAUTHORIZED.getCode());

        logger.error("UnauthorizedExceptionHandler finally, response[{}]", response);

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
    }

    @ResponseStatus(
            value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(
            value = {
                Exception.class
            })
    public ResponseEntity<GenericResponse<Void>> exceptionHandler(HttpServletRequest request,
            Exception e) {
        String message = StatusCode.INTERNAL_SERVER_ERROR.getMessage();

        logger.error("ExceptionHandler", e);

        GenericResponse<Void> response = new GenericResponse<>();
        response.setMessage(message);
        response.setCode(StatusCode.INTERNAL_SERVER_ERROR.getCode());

        logger.error("ExceptionHandler finally, response[{}]", response);

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
    }
}
