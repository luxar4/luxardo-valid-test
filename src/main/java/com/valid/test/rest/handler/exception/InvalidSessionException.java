
package com.valid.test.rest.handler.exception;

public class InvalidSessionException extends RuntimeException {
    private static final long serialVersionUID = 9182669055812081328L;

    public InvalidSessionException() {
        super();
    }

    public InvalidSessionException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public InvalidSessionException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidSessionException(String message) {
        super(message);
    }

    public InvalidSessionException(Throwable cause) {
        super(cause);
    }
}
