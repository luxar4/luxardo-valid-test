
package com.valid.test.session;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

import com.valid.test.rest.handler.exception.InvalidSessionException;

@Component
public class SessionRepository {
    private Map<String, Session> sessions = new ConcurrentHashMap<>();

    public Session getById(String id) {
        Session session = sessions.get(id);

        if (session == null)
            throw new InvalidSessionException("invalid session");

        return session;
    }

    public String save(Session session) {
        String id = UUID.randomUUID().toString();

        session.setId(id);
        sessions.put(id, session);

        return id;
    }

    public void delete(String id) {
        sessions.remove(id);
    }
}
