
package com.valid.test.constant;

public class Constants {
    public static final String HEADER_KEY_SESSION_ID = "session-id";

    public static final String PATTERN_PASSWORD =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
}
