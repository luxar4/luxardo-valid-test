
package com.valid.test.constant;

public enum StatusCode {
    OK(0, "OK"),

    INTERNAL_SERVER_ERROR(-1, "internal server error"),
    BAD_REQUEST(-1, "bad request"),
    METHOD_NOT_ALLOWED(-1, "method not allowed"),
    UNSUPPORTED_MEDIA_TYPE(-1, "unsupported media type"),
    NOT_ACCEPTABLE(-1, "not acceptable"),
    NOT_FOUND(-1, "not found"),
    SERVICE_UNAVAILABLE(-1, "service unavailable"),
    ACCEPTED(-1, "accepted"),
    UNAUTHORIZED(-1, "Unauthorized");

    private int code;
    private String mesage;

    private StatusCode(int code, String message) {
        this.code = code;
        this.mesage = message;
    }

    public int getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.mesage;
    }
}
